require_relative 'okd/version'
require_relative 'okd/config_maps'
require_relative 'okd/secrets'
require_relative 'okd/deployments'
require_relative 'okd/deployment_config'
require_relative 'okd/image_streams'
# imagestreams
# autoscaler
# service
# routes
# project memebership


module Okd
  class Error < StandardError; end
  # Your code goes here...
end

bearer = 'eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJrdWJlcm5ldGVzL3NlcnZpY2VhY2NvdW50Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9uYW1lc3BhY2UiOiJ0ZWxlZ3JhZiIsImt1YmVybmV0ZXMuaW8vc2VydmljZWFjY291bnQvc2VjcmV0Lm5hbWUiOiJkZWZhdWx0LXRva2VuLTd2cWZmIiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9zZXJ2aWNlLWFjY291bnQubmFtZSI6ImRlZmF1bHQiLCJrdWJlcm5ldGVzLmlvL3NlcnZpY2VhY2NvdW50L3NlcnZpY2UtYWNjb3VudC51aWQiOiI3ODVmYWMxOS0xMjhiLTExZTktYTJmNS0wMDUwNTY4NjFiYzgiLCJzdWIiOiJzeXN0ZW06c2VydmljZWFjY291bnQ6dGVsZWdyYWY6ZGVmYXVsdCJ9.lCM8abOgoHdSSXhi8OxEQJhz9h61a_62tMzN1dETkZKZqK1BSHQzcXZAhJHibgbrEd5Z6ognAjx9l-2bj6O-9-x6AZc4H9iua0GjaiK-oFXzbkkMrwnrHKpwZIC4JGsscT0o2PRGZSKn1W7O4uK2k5WlhLgeYSmOeW35SRFMoAxssCDaT7PcKyirJUOcPb-Psp1N2qeaVGO-W5aALljNiS97igodbHVfgrTtM37kFx5UrTew2idWAdsV0xmq7OCzn07Y9hyBo8Yzkrk6umR7TedCz1nFSjl2CajhK-H4PJiHNq3zRhypuspJZqDxCUPEYEDTs8Zw8T5IM0YrkhHtKQ'
client = OKD::Client.new('ocp-elr-core.optum.com', bearer)
deployment = OKD::Deployments.new(client: client, namespace: 'telegraf')
# deploymentconfig = OKD::DeploymentConfig.new(client: client, namespace: 'telegraf')
# puts deploymentconfig.get('egress-writer')

# puts deploymentconfig.log('egress-writer', {sinceSeconds: 20})
# puts deployment.get

# deploymentconfig.create_instantiate('kube')

secret = OKD::Secrets.new(client: client, namespace: 'telegraf')
# puts secret.get('amqp-credentials')
image = OKD::ImageStreams.new(client: client, namespace: 'telegraf')
puts image.get


# @endpoint = 'ocp-elr-core.optum.com'
# @conn = Faraday.new(
#     url: "https://#{@endpoint}",
#     ssl: {verify: true, ca_file: '/Users/miverso2/Downloads/optum_ca1_full_chain.pem'},
#     headers: {'Content-Type': 'application/json', 'Accept': 'application/json'}
# ) do |conn|
#   conn.response :json, :content_type => /\bjson$/
#   conn.adapter :net_http
#   conn.authorization :Bearer, bearer
# end
#
# resp = client.get('search') do |req|
#   req.params['limit'] = 100
#   # req.body = {query: 'salmon'}.to_json
# end

# puts resp.env.url