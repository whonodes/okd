require_relative 'base'

module OKD
  class ConfigMaps < OKD::Base
    def initialize(options = {})
      (options.key? :client) ? @client = options[:client] : build_client(options[:url], options[:bearer])
      @url = options[:namespace].nil? ? '/api/v1/configmaps' : "/api/v1/namespaces/#{options[:namespace]}/configmaps"
    end
  end
end
