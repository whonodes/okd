require_relative 'base'

module OKD
  class Secrets < OKD::Base
    def initialize(options = {})
      (options.key? :client) ? @client = options[:client] : build_client(options[:url], options[:bearer])
      @url = options[:namespace].nil? ? '/api/v1/secrets' : "/api/v1/namespaces/#{options[:namespace]}/secrets"
    end

    def create(secrets, name, type = 'Opaque')
      secret = {data: {}, metadata: {name: name}, name: name}
      secret[:type] = type
      secrets.each do |k, v|
        secret[:data][k] = Base64.encode64 v
      end
      results = @client.post(@url, secret)
      results.body
    end
  end
end
