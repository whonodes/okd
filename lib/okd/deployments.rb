require_relative 'base'

module OKD
  class Deployments < OKD::Base
    def initialize(options = {})
      (options.key? :client) ? @client = options[:client] : build_client(options[:url], options[:bearer])
      @url = options[:namespace].nil? ? '/apis/extensions/v1beta1/deployments' : "/apis/extensions/v1beta1/namespaces/#{options[:namespace]}/deployments"
    end
  end
end