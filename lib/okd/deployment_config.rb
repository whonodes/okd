require_relative 'base'

module OKD
  class DeploymentConfig < OKD::Base
    def initialize(options = {})
      (options.key? :client) ? @client = options[:client] : build_client(options[:url], options[:bearer])
      @url = options[:namespace].nil? ? '/apis/apps.openshift.io/v1/deploymentconfigs' : "/apis/apps.openshift.io/v1/namespaces/#{options[:namespace]}/deploymentconfigs"
    end

    def get_scale(name)

    end

    def update_scale(name)

    end

    def patch_scale(name)

    end

    def get_status(name)

    end

    def update_status(name)

    end

    def patch_status(name)

    end

    def log(name, options = {})
      results = @client.get(@url.concat "/#{name}/log") do |req|
        # puts 'test'
        # puts req.methods(false)
        # req.params = options
        # req.params['sinceSeconds'] = 1
        # req.params['sinceSeconds'] = '1'
        # req.params['version'] = 85
      end
      # puts results.env.url
      # puts results.env.params
      results.body
    end

    def rollback(name)

    end

    def create_instantiate(name)
      body = {kind: 'DeploymentRequest', apiVersion: 'apps.openshift.io/v1'}
      results = @client.post(@url.concat "/#{name}/instantiate", MultiJson.dump(body))
      results.body
    end
  end
end