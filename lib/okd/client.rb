require 'faraday'
require 'multi_json'
require 'faraday_middleware'
require 'base64'

module OKD
  class Client
    def initialize(endpoint, bearer, opts = {})
      @conn = Faraday.new(
          url: "https://#{endpoint}",
          ssl: {verify: true, ca_file: '/Users/miverso2/Downloads/optum_ca1_full_chain.pem'},
          headers: {'Content-Type': 'application/json', 'Accept': 'application/json'}
      ) do |conn|
        #conn.response :json, :content_type => /\bjson$/
        conn.adapter :net_http
        conn.authorization :Bearer, bearer
      end
    end

    def post(endpoint, payload)
      @conn.post do |req|
        req.url endpoint
        req.options.timeout = 5
        req.options.write_timeout = 5
        req.options.open_timeout = 1
        req.body = (payload.is_a? String) ? payload : MultiJson.dump(payload)
      end
    end

    def patch(endpoint, payload)
      @conn.patch do |req|
        req.url endpoint
        req.options.timeout = 5
        req.options.write_timeout = 5
        req.options.open_timeout = 1
        req.headers['Content-Type'] = 'application/json-patch+json'
        req.body = (payload.is_a? String) ? payload : MultiJson.dump(payload)
      end
    end

    def get(endpoint)
      @conn.get do |req|
        req.url endpoint
        req.options.timeout = 5
        req.options.open_timeout = 1
      end
    end

    def put(endpoint, payload)
      @conn.put do |req|
        req.url endpoint
        req.options.timeout = 5
        req.options.write_timeout = 5
        req.options.open_timeout = 1
        req.body = (payload.is_a? String) ? payload : MultiJson.dump(payload)
      end
    end

    def delete(endpoint)
      @conn.delete do |req|
        req.url endpoint
        req.options.timeout = 5
        req.options.write_timeout = 5
        req.options.open_timeout = 1
      end
    end

    def head(endpoint)
      @conn.head do |req|
        req.url endpoint
        req.options.timeout = 5
        req.options.open_timeout = 1
      end
    end

    def options(endpoint)
      @conn.options do |req|
        req.url endpoint
        req.options.timeout = 5
        req.options.open_timeout = 1
      end
    end
  end
end

# bearer = 'eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJrdWJlcm5ldGVzL3NlcnZpY2VhY2NvdW50Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9uYW1lc3BhY2UiOiJ0ZWxlZ3JhZiIsImt1YmVybmV0ZXMuaW8vc2VydmljZWFjY291bnQvc2VjcmV0Lm5hbWUiOiJkZWZhdWx0LXRva2VuLTd2cWZmIiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9zZXJ2aWNlLWFjY291bnQubmFtZSI6ImRlZmF1bHQiLCJrdWJlcm5ldGVzLmlvL3NlcnZpY2VhY2NvdW50L3NlcnZpY2UtYWNjb3VudC51aWQiOiI3ODVmYWMxOS0xMjhiLTExZTktYTJmNS0wMDUwNTY4NjFiYzgiLCJzdWIiOiJzeXN0ZW06c2VydmljZWFjY291bnQ6dGVsZWdyYWY6ZGVmYXVsdCJ9.lCM8abOgoHdSSXhi8OxEQJhz9h61a_62tMzN1dETkZKZqK1BSHQzcXZAhJHibgbrEd5Z6ognAjx9l-2bj6O-9-x6AZc4H9iua0GjaiK-oFXzbkkMrwnrHKpwZIC4JGsscT0o2PRGZSKn1W7O4uK2k5WlhLgeYSmOeW35SRFMoAxssCDaT7PcKyirJUOcPb-Psp1N2qeaVGO-W5aALljNiS97igodbHVfgrTtM37kFx5UrTew2idWAdsV0xmq7OCzn07Y9hyBo8Yzkrk6umR7TedCz1nFSjl2CajhK-H4PJiHNq3zRhypuspJZqDxCUPEYEDTs8Zw8T5IM0YrkhHtKQ'
#
#
# foo = OKD::Client.new('ocp-elr-core.optum.com', bearer)
# # puts foo.get('/api/v1/namespaces/telegraf/secrets/amqp-credentials').body
#
# body = {data: {test: Base64.encode64('encoded string'), test2: Base64.encode64('second string')}, kind: 'Secret', metadata: {'name': 'test', 'namespace': 'telegraf'}, type: 'Opaque'}
# patch_body = {data: {test2: Base64.encode64('second string')}, kind: 'Secret', metadata: {'name': 'test', 'namespace': 'telegraf'}, type: 'Opaque'}
# patch_body = {"op":"add", "path": "/test2", "value": Base64.encode64('second string')}
# patch_body = '[{"op":"add","path":"/data/test2","value":"dGhpcyBpcyBteSBzZWNvbmQgc3RyaW5n"}]'
# puts patch_body
# response = foo.patch('/api/v1/namespaces/telegraf/secrets/test', patch_body)
#
# puts response.status
# puts response.body

