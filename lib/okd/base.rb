require_relative 'client'

module OKD
  class Base
    def build_client(url, bearer)
      @client = OKD::Client.new(url, bearer)
    end

    def create(body)
      results = @client.post(@url, body)
      results.body
    end

    def get(name = nil)
      results = (name.nil?) ? @client.get(@url) : @client.get(@url.concat "/#{name}")
      results.body
    end

    def patch

    end

    def update

    end

    def delete(name)
      results = (name.nil?) ? @client.delete(@url) : @client.delete(@url.concat "/#{name}")
      results.body
    end
  end
end