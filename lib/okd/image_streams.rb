require_relative 'base'

module OKD
  class ImageStreams < OKD::Base
    def initialize(options = {})
      (options.key? :client) ? @client = options[:client] : build_client(options[:url], options[:bearer])
      @url = options[:namespace].nil? ? '/apis/image.openshift.io/v1' : "/apis/image.openshift.io/v1/namespaces/#{options[:namespace]}/imagestreams"
    end
  end
end
